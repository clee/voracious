# Voracious

![Voracious screenshot](https://mg8.org/voracious/screenshot.png)

Voracious is an ebook reader; currently only ePub-formatted ebooks are supported.

Voracious is written in Vala using GTK3, WebKit2, Gee, and GXML.

(Note that at the moment, you'll need to build GXML from git [with a patch applied](https://gitlab.gnome.org/GNOME/gxml/merge_requests/2) if you want to use Voracious.))
