using GLib;
using Gtk;
using WebKit;

class Voracious : Window {
	public Voracious(string[] epub_paths) {
		title = "Voracious";
		set_size_request(800, 480);

		var b = new Box(Orientation.VERTICAL, 0);
		add(b);

		var model = new Gtk.ListStore(3, typeof(Gdk.Pixbuf), typeof(string), typeof(Epub));

		var view = new IconView.with_model(model);
		view.set_pixbuf_column(0);
		view.set_text_column(1);
		view.set_selection_mode(Gtk.SelectionMode.SINGLE);
		var s = new ScrolledWindow(null, null);
		s.set_policy(PolicyType.NEVER, PolicyType.AUTOMATIC);
		s.add(view);
		b.pack_start(s);

		TreeIter iter;
		foreach (string path in epub_paths) {
			var e = new Epub(path);
			e.load();
			model.append(out iter);
			model.set(iter, 0, e.cover, 1, e.title, 2, e);
		}

		view.item_activated.connect((p) => {
			Value v;
			TreeIter i;
			model.get_iter(out i, p);
			model.get_value(i, 2, out v);
			Epub e = (Epub)v;
			var epubWindow = new EpubWindow(e);
			epubWindow.show_all();
		});
		destroy.connect(Gtk.main_quit);
	}
}

public class EpubWindow : Window {
	private Epub epub;
	private EpubInternal ei;

	public EpubWindow(Epub e) {
		epub = e;
		ei = new EpubInternal(e.filename);
		set_size_request(800, 480);

		Box b = new Box(Orientation.VERTICAL, 0);
		add(b);

		HeaderBar h = new HeaderBar();
		h.set_title(e.title);
		h.set_show_close_button(true);
		set_titlebar(h);

		ScrolledWindow s = new ScrolledWindow(null, null);
		s.set_policy(PolicyType.AUTOMATIC, PolicyType.AUTOMATIC);
		s.add(create_renderer());
		s.get_vscrollbar().set_visible(true);
		s.get_hscrollbar().set_visible(false);
		b.pack_start(s);
	}

	private WebContext ebook_context() {
		var webctx = new WebContext();
		webctx.register_uri_scheme("ebook", (ebookRequest) => {
			var path = ebookRequest.get_path();
			if (path.has_prefix("/")) {
				path = path[1:path.length];
			}

			var contents = ei.get_bytearray(path);
			ebookRequest.finish(new MemoryInputStream.from_data(contents.data, GLib.free),
			                    contents.len,
			                    null);
		});
		return webctx;
	}

	private WebView create_renderer() {
		var spine = epub.spine;

		var view = new WebView.with_context(ebook_context());
		debug("loading " + spine[0]);

		var book = "";
		foreach (string path in spine) {
			debug("loading " + path);
			book += ei[path];
		}

		view.load_html(book, @"ebook:///$(spine[0])");
		return view;
	}
}

public static int main(string[] args) {
	Gtk.init(ref args);

	var app = new Voracious(args[1:args.length]);
	app.show_all();

	Gtk.main();
	return 0;
}
