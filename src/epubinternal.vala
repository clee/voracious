using Gee;

public string uint8_array_to_string(uint8[] data, int length = -1) {
	if (length < 0)
		length = data.length;

	StringBuilder builder = new StringBuilder();
	for (int ctr = 0; ctr < length; ctr++) {
		if (data[ctr] != '\0')
			builder.append_c((char)data[ctr]);
		else
			break;
	}

	return builder.str;
}

private class EpubInternal : Object {
	public TreeMap<string, GLib.ByteArray> contents;

	public EpubInternal(string filename) {
		var r = new Archive.Read();
		contents = new TreeMap<string, GLib.ByteArray>();
		r.support_format_all();
		r.support_filter_all();
		r.open_filename(filename, 8192);
		unowned Archive.Entry e;
		while (r.next_header(out e) == Archive.Result.OK) {
			debug("[%s]<%lld>", e.pathname(), e.size());
			uint8[] buf = new uint8[e.size()];
			r.read_data(buf, (size_t)e.size());
			var ba = new GLib.ByteArray.take(buf);
			contents[e.pathname()] = ba;
		}
		r.close();
	}

	~EpubInternal() {
		debug("killing this epubinternal");
	}

	public new string? get(string key) {
		if (contents.has_key(key)) {
			return uint8_array_to_string(contents[key].data);
		}
		return null;
	}

	public new GLib.ByteArray? get_bytearray(string key) {
		if (contents.has_key(key)) {
			return contents[key];
		}

		return new GLib.ByteArray.sized(0);
	}
}
